#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

struct Product
{
    string name;
    float price;
};

void Program2()
{
    cout << endl << "POINTERS AND OBJECTS" << endl;

    Product products[3];
    products[0].name = "Pencil";
    products[0].price = 1.59;

    products[1].name = "Markers";
    products[1].price = 2.39;

    products[2].name = "Eraser";
    products[2].price = 0.59;

    Product* ptrProduct = nullptr;

    cout << endl << "PRODUCTS:" << endl;
    for ( int i = 0; i < 3; i++ )
    {
        cout << left
            << setw( 5 ) << i
            << setw( 20 ) << products[i].name
            << setw( 10 ) << products[i].price << endl;
    }

    // 1. TODO: Ask user to enter the INDEX of an item to modify. Store in an int variable.
    // DO HERE

    // 2. TODO: Assign ptrProduct to the address of the element at that INDEX.
    // DO HERE

    // 3. TODO: Ask the user to enter a new price for the item. Have them overwrite the price via the ptrProduct pointer.
    // DO HERE

    // 4. TODO: Ask the user to enter a new name for the item. Have them overwrite the name via the ptrProduct pointer.
    // DO HERE


    cout << endl << "UPDATED PRODUCTS:" << endl;
    for (int i = 0; i < 3; i++)
    {
        cout << left
            << setw(5) << i
            << setw(20) << products[i].name
            << setw(10) << products[i].price << endl;
    }

    cout << endl << "THE END" << endl;
}

/* EXAMPLE OUTPUT:
POINTERS AND OBJECTS

PRODUCTS:
0    Pencil              1.59      
1    Markers             2.39      
2    Eraser              0.59      

Enter index of item to modify: 0
Enter new price: $3.29
Enter new name: Notebook

UPDATED PRODUCTS:
0    Notebook            3.29      
1    Markers             2.39      
2    Eraser              0.59      

THE END
*/