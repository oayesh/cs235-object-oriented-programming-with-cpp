#include <iostream>
#include <string>
using namespace std;

/*
* Declaring a pointer:          DATATYPE* PTRVAR = nullptr;
* Get address of variable:      &VARIABLE
* Assigning address:            PTRVAR = &OTHERVARIABLE;
* Displays pointed-to address:  cout << PTRVAR;
* Displays pointed-to value:    cout << *PTRVAR;
*/
void Program1()
{
    cout << endl << "EXPLORING ADDRESSES" << endl;

    int num1 = 10, num2 = 15, num3 = -5;

    cout << endl << "Original Variables!" << endl;
    // 1. TODO: Display each variable's name, address, and current value
    cout << "num1: " << endl;
    cout << "num2: " << endl;
    cout << "num3: " << endl;

    cout << endl << "Pointer!" << endl;
    // 2. TODO: Create an integer pointer, initialize it to nullptr.
    // ADD HERE
    
    // 3. TODO: Display the address that the pointer is pointing to.
    // ADD HERE

    cout << "Don't dereference the nullptr!!" << endl;

    // 4. TODO: Point the pointer to the address of num1.
    // ADD HERE

    // 5. TODO: Display "ptr is pointing to address", then the address of where it is pointing to.
    // ADD HERE

    // 6. TODO: Display "The value at that address is", and then dereference the pointer to display the pointed-to value.
    // ADD HERE

    // 7. TODO: Ask the user to enter a new integer value for this variable. Overwrite the data via the pointer by using dereferencing.
    // ADD HERE

    // 8. TODO: Repeat steps 4 - 7 for num2 and num3 variables.

    cout << endl << "Changed Variables!" << endl;
    // 9. TODO: Copy step 1 below to show the updated variable values.

    cout << endl << "THE END" << endl;
}

/*
* EXAMPLE OUTPUT:
Run which program? (1-5): 1

EXPLORING ADDRESSES

Original Variables!
num1's address is 0x7ffc44cc66b4 and its value is 10
num2's address is 0x7ffc44cc66b8 and its value is 15
num3's address is 0x7ffc44cc66bc and its value is -5

Pointer!
ptr is pointing to address 0
Don't dereference the nullptr!!

ptr is pointing to address 0x7ffc44cc66b4
The value at that address is 10
Enter a new value for it: 1 

ptr is pointing to address 0x7ffc44cc66b8
The value at that address is 15
Enter a new value for it: 2

ptr is pointing to address 0x7ffc44cc66bc
The value at that address is -5
Enter a new value for it: 3


Changed Variables!
num1's address is 0x7ffc44cc66b4 and its value is 1
num2's address is 0x7ffc44cc66b8 and its value is 2
num3's address is 0x7ffc44cc66bc and its value is 3

THE END
*/