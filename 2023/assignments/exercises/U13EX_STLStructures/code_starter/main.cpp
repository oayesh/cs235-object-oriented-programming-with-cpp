#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

void TraditionalArrayProgram();
void ArrayObjectProgram();
void VectorProgram();
void StackProgram();
void QueueProgram();
void MapProgram();
void ListProgram();

int main()
{
    srand(time(NULL));

    cout << "1. Traditional-Array program" << endl;
    cout << "2. Array-Object program" << endl;
    cout << "3. Vector program" << endl;
    cout << "4. List program" << endl;
    cout << "5. Map program" << endl;
    cout << "6. Stack program" << endl;
    cout << "7. Queue program" << endl;
    cout << endl << "Run which one? ";
    int prog;
    cin >> prog;

    switch (prog)
    {
    case 1: TraditionalArrayProgram(); break;
    case 2: ArrayObjectProgram(); break;
    case 3: VectorProgram(); break;
    case 4: ListProgram(); break;
    case 5: MapProgram(); break;
    case 6: StackProgram(); break;
    case 7: QueueProgram(); break;
    }

	return 0;
}