#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
using namespace std;

struct Product
{
    Product() {}

    Product( string name, float price )
    {
        this->name = name;
        this->price = price;
    }

    void Display()
    {
        cout << setw( 20 ) << name << "\t $" << price << endl;
    }

    string name;
    float price;
};

void Program2()
{
    // TODO: Create program
}

