clear
echo "-------------------------------------------------"
echo "Build program..."
g++ *.h *.cpp -o u05ex_exe
echo "-------------------------------------------------"
echo "TEST: Run program"
echo ""
echo "EXPECTED OUTPUT:"
echo "1. Four questions are asked, some multiple choice and some fill in the blank."
echo "2. A score is displayed at the end."
echo ""
echo "ACTUAL OUTPUT:"
echo 1 2 1 3 | ./u05ex_exe
echo ""
echo "-------------------------------------------------"
echo "PROGRAM CODE:"
echo "- u05ex.cpp -"
cat u05ex.cpp
echo ""
echo "- Questions.h -"
cat Questions.h
echo ""
echo "- Questions.cpp -"
cat Questions.cpp
echo ""
